import axios from 'axios';
// Käytetään tekemään api-kutsuja

const url = 'http://localhost:5000/posts';
// Palauttaa kaikki postit mitä löytyy databasesta

export const fetchPosts = () =>  axios.get(url);
export const createPost = (newPost) => axios.post(url, newPost);
export const updatePost = (id, updatedPost) => axios.patch(`${url}/${id}`, updatedPost);
