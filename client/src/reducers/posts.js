const reducer = (posts = [], action) => {
	switch (action.type) {
		case 'FETCH_ALL':
			// return posts;
			// Actual posts:
			return action.payload;
		case 'CREATE':
			// return posts;
			return [...posts, action.payload];
		case 'UPDATE':
			// action.payload is the updated post
			// Jos postin id on päivitetyn postin id, niin palautetaan päivitetty posti
			// Muuten palautetaan posti postina
			return posts.map((post) => post._id === action.payload._id ? action.payload : post);
		default:
			return posts;
	}
}

export default reducer;
