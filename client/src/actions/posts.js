import * as api from '../api';

// Action Creators --> functions that return actions
// Hox: asynkronisuuden takia kestää aikaa että saadaan kaikki fetchattua --> redux-thunk apuun
// redux-thunkin kanssa returnin sijaan täytyy dispatchata
export const getPosts = () => async (dispatch) => {
	try {
		// data on posts
		const { data } = await api.fetchPosts();

		dispatch({ type: 'FETCH_ALL', payload: data});
	} catch (error) {
		console.log(error);
	}

	// Nämä siirrettiin try catchin alle
	// const action = { type: 'FETCH_ALL', payload: [] }
	// dispatch(action);
}

export const createPost = (post) => async (dispatch) => {
	try {
		const { data } = await api.createPost(post);

		dispatch({ type: 'CREATE', payload: data });
	} catch (error) {
		console.log(error);
	}
}

export const updatePost = (id, post) => async (dispatch) => {
	try {
		const { data } = await api.updatePost(id, post);

		dispatch({ type: 'UPDATE', payload: data });
	} catch (error) {
		console.log(error);
	}
}
