import mongoose from 'mongoose';

// Uniformi erilaisille tiedostomuodoille (on kuvaa, tekstiä jne.)
const postSchema = mongoose.Schema({
	title: String,
	message: String,
	creator: String,
	tags: [String],
	selectedFile: String,
	likeCount: {
		type: Number,
		default: 0
	},
	createdAt: {
		type: Date,
		default: new Date()
	}
});

const PostMessage = mongoose.model('PostMessage', postSchema);

// Exportataan mongoose-model, jotta voidaan myöhemmin ajaa find/create/delete/jne komentoja
export default PostMessage;
