import PostMessage from '../models/postMessage.js';
import express from 'express';
import mongoose from 'mongoose';

// Pitää exportata että voidaan käyttää routessissa
export const getPosts = async (req, res) => {
	try {
		// Findissa kestää aikaa, se on asynkroninen funktio:
		// Täytyy lisätä await --> jonka takia getPostista pitää tehhä asynkroninen
		const postMessages = await PostMessage.find();

		res.status(200).json(postMessages);
	} catch (error) {
		res.status(404).json({ message: error.message });
	}
}

/**
 * Luodaan postaus
 * Tämän jälkeen täytyy heti mennä frontin puolelle luomaan formi
 */
export const createPost = async (req, res) => {
	const post = req.body;

	// Saadaan frontin avulla
	const newPost = new PostMessage(post);

	// Jälleen asynkroninen (await), joten pitää lisätä funkioon async
	try {
		await newPost.save();

		// 201 tarkoittaa onnistunutta luomista
		// restapitutorial.com/httpstatuscodes.html
		res.status(201).json(newPost);
	} catch (error) {
		res.status(409).json({ message: error.message });
	}
}

/**
 * Update posts
 */
export const updatePost = async (req, res) => {
	const { id: _id } = req.params; // Routes-kansiossa lisätty id mukaan, saadaan parametrinä se (/posts/123), Mongoose-id -muotoon
	const post = req.body;

	// Tsekataan onko id oikeanlainen
	if(!mongoose.Types.ObjectId.isValid(_id)) return res.status(404).send('No post with that id');

	// Jälleen asynkroninen, pitää lisätä await eteen, huomaa että postin lisäksi täytyy ottaa myös id mukaan vanhasta postista
	const updatedPost = await PostMessage.findByIdAndUpdate(_id, {...post, _id}, { new: true });

	res.json(updatedPost);
}