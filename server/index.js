import express from 'express';
import mongoose from 'mongoose';
import cors from 'cors';
import dotenv from 'dotenv';

import postRoutes from './routes/posts.js';

const app = express();
dotenv.config();

app.use(express.json({
	limit: "30mb",
	extended: true
}));
app.use(express.urlencoded({
	limit: "30mb",
	extended: true
}));
app.use(cors());

app.use('/posts', postRoutes);

// FYI: Salasana kirjoitettu suoraan muuttujaan mukaan yksinkertaistuksen vuoksi
const CONNECTION = process.env.CONNECTION_URL || 'mongodb+srv://test:test123@memories.rvf5x.mongodb.net/myFirstDatabase?retryWrites=true&w=majority'
const PORT = process.env.PORT || 5000;

mongoose.connect(CONNECTION, { useNewUrlParser: true, useUnifiedTopology: true })
	.then(() =>  app.listen(PORT, () => console.log(`Server running on port: ${PORT}`)))
	.catch((error) => console.log(error.message));

mongoose.set('useFindAndModify', false);
