import express from 'express';

// localhost:5000/posts
// Ei logiikkaa tänne, halutaan pitää tiedosto lyhyenä
// Logiikka controllerin puolella
// Nodessa pitää laittaa tiedostopääte mukaan importtiin

import { getPosts, createPost, updatePost } from '../controllers/posts.js';

const router = express.Router();

router.get('/', getPosts);
router.post('/', createPost);
router.patch('/:id', updatePost); // Update posts, hox id!

export default router;

